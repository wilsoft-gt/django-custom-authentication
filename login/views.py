from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse

#THIS 3 MODULES DO ALL THE TRICK
from django.contrib.auth import authenticate, login, logout

#HERE YOU LOAD THIS DECORATOR TO DECIDE WHICH VIEWS REQUIRE LOGIN
from django.contrib.auth.decorators import login_required

def loginView(request):
    if request.method == "GET":
        return render(request, 'login/login.html')
        
    if request.method == "POST":
        #once the user post the form we will get the values
        
        #THE 'next' PARAMETER WILL BE PROVIDED BY THE AUTH SYSTEM
        nextUrl = request.GET.get('next')
        
        #this will be the username, if your input name is user then should be request.GET.get('user') etc
        username = request.POST.get('username')
        #here we get the password
        password = request.POST.get('password')
        
        
        #now we check if the credentiasl are valid and if valid authenticate will return the user object
        user = authenticate(request=any, username=username, password=password)
        
        #now check if the user was authenticated and returned an object
        if user is not None:
            #if the user was returned then login the user and start the session
            login(request, user)
            #redirect to the main page, @login_required add an extra parameter 'next' that you can get if you need to redirect to a different page
            return HttpResponseRedirect(nextUrl)
        else:
            #if the user was empty then the credentials are incorrect
            return render(request, 'login/error.html')
            
            
def logouView(request):
    logout(request)
    return HttpResponseRedirect('/')

#this view only can be viewed if the user is authenticated, will redirect to login and then will provide a "next" parameter on the url
@login_required
def secretView(request):
    return render(request, 'login/secretView.html')
    
#this view can be accessed without authentication
def anyView(request):
    return render(request, 'login/main.html')
    