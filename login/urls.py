from django.contrib import admin
from django.urls import path
from login.views import *
urlpatterns = [
    path('login/', loginView, name='loginview'),
    path('logout/', logouView, name='logout'),
    path('secretUrl', secretView, name='secretUrl'),
    path('', anyView, name='anyview')
    
]
